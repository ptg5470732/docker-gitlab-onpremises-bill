# Docker installation

- Linux Rocky

```bash
# Update the System:
sudo dnf update

# Install Required Dependencies:
sudo dnf install -y dnf-plugins-core

# Add the Docker repository to the system using the dnf command.
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Now, install the Docker Engine package using dnf.
sudo dnf install -y docker-ce docker-ce-cli containerd.io

# Start the Docker service and enable it to start on system boot.
sudo systemctl start docker
sudo systemctl enable docker

# Verify Docker Installation:
sudo docker --version

# Manage Docker as a Non-root User (Optional):
sudo usermod -aG docker $USER
```

- Linux ubuntu

```bash
   sudo apt update

   sudo apt install apt-transport-https ca-certificates curl software-properties-common

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

   sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

   apt-cache policy docker-ce

   sudo apt install docker-ce

   sudo systemctl status docker
```

Ref:
[Docker-Installation](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
